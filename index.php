<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Худякова Нелли Константиновна 181-322 №A-5</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,500" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<header>
    <div class="header-logo"></div>
    <div class="header-heading"><h1>Худякова Нелли Константиновна 181-322 №A-5</h1></div>
    <div></div>
</header>

<main>
    <div id="main_menu"><?php
//кнопка выбора для табличной верстки
         echo '<a href="?html_type=TABLE';
         if (isset($_GET['content'])) echo '&content='.$_GET['content'];

         echo '"';

         if (array_key_exists('html_type', $_GET)&&$_GET['html_type']=='TABLE')
             echo 'class="selected"';

         echo '>Табличная верстка</a>';

//кнопка выбора для блочной верстки
        echo '<a href="?html_type=DIV';
        if (isset($_GET['content'])) echo '&content='.$_GET['content'];
        echo '"';

        if (array_key_exists('html_type', $_GET)&&$_GET['html_type']=='DIV')
            echo 'class="selected"';

        echo '>Блочная верстка</a>';

    ?></div>

    <div id="product_menu"><?php
//кнопки выбора содержания

        echo '<a href="/php-lab5/index.php"';
        if (!isset($_GET['content'])) echo 'class="selected"';
        echo '>Вся таблица умножения</a>';

        for ($i=2; $i<=9; $i++){
            if (!array_key_exists('html_type', $_GET))
            echo '<a href="?content='.$i.'&html_type=TABLE'.'"';
            else echo '<a href="?content='.$i.'&html_type='.$_GET['html_type'].'"';
//            echo '<a href="?content='.$i.'"';

            if(isset($_GET['content'])&&$_GET['content']==$i) echo 'class="selected"';
            echo '>Таблица умножения на '.$i.'</a>';
        }
    ?></div>

    <div id="multiplication_table">
        <?php
        function outNumAsLink($m){
            if (($m>=2)&&($m<=9)){
                if (isset($_GET['html_type'])){
                    $output= '<a href="?content='.$m.'&html_type='.$_GET['html_type'].'">'.$m.'</a>';
                } else  $output= '<a href="?content='.$m.'">'.$m.'</a>';
//                $output= '<a href="?content='.$m.'">'.$m.'</a>';
            } else
                $output= $m;

            return $output;
        }
        function outRow ($n){
            for ($i=2; $i<=9; $i++)
                echo '<div class="div-cell">'.outNumAsLink($n).'x'.outNumAsLink($i).'='.outNumAsLink($i*$n).'</div>';
        }
        function outTableRow ($n){
            for ($i=2; $i<=9; $i++)
                echo '<td>'.outNumAsLink($n).'x'.outNumAsLink($i).'='.outNumAsLink($i*$n).'</td>';
        }

        function outTableForm(){
            echo '<table>';
            if (!isset($_GET['content'])){
                for ($i=2; $i<=9; $i++){
                    echo '<tr>';
                    outTableRow($i);
                    echo '</tr>';
                }
            } else {
                echo '<tr>';
                outTableRow($_GET['content']);
                echo  '</tr>';
            }
            echo '</table>';
        }

        function outDivForm(){
            if (!isset($_GET['content'])){
                for ($i=2; $i<=9; $i++){
                    echo '<div class="ttRow">';
                    outRow($i);
                    echo '</div>';
                }
            } else {
                echo '<div class="ttSingleRow">';
                outRow($_GET['content']);
                echo  '</div>';
            }
        }
        if ((!isset($_GET['html_type']))||($_GET['html_type']=='TABLE'))
            outTableForm();
        else
            outDivForm();
    ?>
    </div>
</main>

<footer>
    <p>Тип верстки:
    <?php
    if (!isset($_GET['html_type']))
        echo 'тип верстки не выбран';
    else if ($_GET['html_type']=='TABLE')
        echo 'табличная';
    else if ($_GET['html_type']=='DIV')
        echo 'блочная';
    ?>
    </p>

    <p>Содержание таблицы:
        <?php
        if (isset($_GET['content']))
            echo 'таблица умножения на '.$_GET['content'];
        else echo 'вся таблица умножения';
        ?>
    </p>

    <p>Сформировано:
        <?php
        date_default_timezone_set('Europe/Moscow');
        $date = date("d.m.y H:i:s");
        echo $date;
        ?>
    </p>
</footer>

</body>
</html>